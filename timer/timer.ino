void setup() {
  pinMode(LED_BUILTIN, OUTPUT);

  cli();
  TCCR1A = 0;
  TCCR1B = 0;
  
  // (target time) = (timer resolution) * (# timer counts + 1)
  // time resolution = 16e6 / prescaler

  // timer counts
  OCR1A = 4687;

  // reset timer by coincidence
  TCCR1B |= (1 << WGM12);

  // set prescaler = 1024
  TCCR1B |= (1 << CS10) | (1 << CS12);

  TIMSK1 |= (1 << OCIE1A);
  sei();
}

ISR(TIMER1_COMPA_vect) {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}

void loop() {}
