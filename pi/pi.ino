void setup() {
  Serial.begin(9600);
}

void loop() {
  delay(500);

  /*
  * Both variants are good: v1 and v2
  * However v1 approximates better than v2
  */

  Serial.print("V1: ");
  v1();
  delay(100);

  Serial.print("V2: ");
  v2();
  delay(100);

  Serial.println();
}


void v1() {
  unsigned long iters = 1000000;
  float d = 1.;
  float pi = 0;
  float a = 4.;
  unsigned long start = millis();
  for (unsigned long i = 1; i < iters; i++) {
    pi += a / d;
    a = -a;
    d += 2.;
  }
  Serial.println(pi);
  Serial.println(millis() - start);
}

void v2() {
  unsigned long iters = 1000000;
  float d = 1.;
  float pi = 0;
  float a = 1.;
  unsigned long start = millis();
  for (unsigned long i = 1; i < iters; i++) {
    pi += a / d;
    a = -a;
    d += 2.;
  }
    Serial.println(pi);
Serial.println(millis() - start);
} 
